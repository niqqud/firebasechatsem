//
//  Utils.swift
//  FirebaseChat
//
//  Created by RoHIT on 28/05/19.
//  Copyright © 2019 RoHIT. All rights reserved.
//

import Foundation
struct Utils {
    
    static var shared = Utils()
    private init(){ }
    
    //For debug purpose
    func log(data: String){
        print("----------------------")
        print("Debug: => \(data)")
        print("----------------------")
    }
    
    //For debugging error
    func errorLog(error: Error){
        print("----------------------")
        print("Debug Error: => \(error.localizedDescription) ")
        print("----------------------")
    }
    
    
}
