//
//  FirebaseService.swift
//  FirebaseChat
//
//  Created by RoHIT on 28/05/19.
//  Copyright © 2019 RoHIT. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
class FirebaseService {
    static let shared = FirebaseService()
    private init(){}
    
    let firebaseDatabase: DatabaseReference = Database.database().reference()
    let firebaseStorage: StorageReference = Storage.storage().reference()
    
    func createUser(emailID: String, password: String = "12345678",userInfo: [String: Any],handler: @escaping (Bool,FirebaseEnum,String?) -> ()){
        Auth.auth().useAppLanguage()
        Auth.auth().createUser(withEmail: emailID, password: password) {[weak self] (authData, error) in
            if error == nil {
                guard let strongSelf = self else {return}
                guard let authUser =  authData else { return }
                strongSelf.createUserProfile(userID:authUser.user.uid,userInformation: userInfo,handler: handler)
            }else{
                Utils.shared.errorLog(error: error!)
                handler(false,.FirebaseUserError,nil)
            }
        }
    }
    
    
    
    private func createUserProfile(userID: String,userInformation: [String: Any],handler: @escaping (Bool,FirebaseEnum,String?) -> ()){
            var userInfo = userInformation
            userInfo["UID"] = userID
        firebaseDatabase.child(FirebaseEnum.FirebaseUsers.rawValue).child(userID).setValue(userInfo) { (error, databaseRef) in
            if error == nil{
                handler(true,.FirebaseUserSuccess,userID)
            }else{
                handler(false,.FirebaseDatabaseUserError,nil)
            }
        }
    }
    
    
    func loginUser(emailID: String, password: String = "12345678", handler: @escaping (Bool,FirebaseEnum,String?) -> ()){
        Auth.auth().signIn(withEmail: emailID, password: password) { [weak self] (authData, error) in
            if error == nil {
                guard let strongSelf = self else {return}
                guard let authUser =  authData else { return }
                strongSelf.updatingUserProfile(userID: authUser.user.uid, handler: handler)
            }else{
                handler(false,.FirebaseDatabaseUserError,nil)
            }
        }
    }
    
    
    
    
   private func updatingUserProfile(userID: String,handler: @escaping (Bool,FirebaseEnum,String?) -> ()){
        let lastSeenPath = "\(FirebaseEnum.FirebaseUsers.rawValue)/\(userID)/\(FirebaseEnum.fireLastSeen.rawValue)"
        firebaseDatabase.child(lastSeenPath).setValue(Date().description) { (error, databaseRef) in
            if error == nil{
                handler(true,.FirebaseUserUpdateSuccess,userID)
            }else{
                handler(false,.FirebaseDatabaseUserUpdateError,nil)
            }
        }
    }
    
    
    func checkLoginStatus(handler: @escaping (Bool,FirebaseEnum) -> ()){
        if Auth.auth().currentUser != nil {
           return handler(true,.FireUserLoginStatus)
        } else {
           return handler(false,.FireUserLoginFailStatus)
        }

    }
    
    
    
    func logoutUser(handler: @escaping (Bool,FirebaseEnum) -> ()){
        do {
           try Auth.auth().signOut()
            handler(true,.FirebaseUsersLogoutSuccess)
        }catch{
            handler(false,.FirebaseUsersLogoutFailure)
        }
    }
    
    
   

    func getUsersList(handler: @escaping (Bool,FirebaseEnum,[Users]?) -> ()){
        firebaseDatabase.child(FirebaseEnum.FirebaseUsers.rawValue).observeSingleEvent(of: .value) { (snapshot) in
            var users: [Users] = []
            if let snap = snapshot.value as? [String: [String: Any]]{
                for (_,value) in snap{
                    let jsonRequest = try! JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                  let user =   try! JSONDecoder().decode(Users.self, from: jsonRequest)
                    users.append(user)
                }
                handler(true,.FirebaseUsersListSuccess,users)
            }else{
                handler(false,.FirebaseUsersListFailure,nil)
            }
        }
    }
    
    func getGroupsList(UserID: String,handler: @escaping (Bool,FirebaseEnum,[Groups]?) -> ()){
        firebaseDatabase.child("Groups").observeSingleEvent(of: DataEventType.value) { (snapshot) in
            var groups: [Groups] = []
            if let snap = snapshot.value as? [String: [String: Any]]{
                for (_,value) in snap{
                    let jsonRequest = try! JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                    let userGroup =   try! JSONDecoder().decode(Groups.self, from: jsonRequest)
                    groups.append(userGroup)
                }
                handler(true,.FirebaseUsersListSuccess,groups)
            }else{
                handler(false,.FirebaseUsersListFailure,nil)
            }
        }
    }
    
    
    func createGroup(groupTitle: String,group: [String: Any],handler: @escaping (Bool,FirebaseEnum) -> ()){
        firebaseDatabase.child("Groups").child(groupTitle).setValue(group) {[weak self] (error, databaseRef) in
            if error == nil{
                guard let strongSelf = self else { return }
                strongSelf.createMessages(groupTitle: groupTitle, handler: handler)
            }else{
                handler(false,.FirebaseGroupCreationError)
            }
        }
    }
    
    
    private func createMessages(groupTitle: String,handler: @escaping (Bool,FirebaseEnum) -> ()){
        
        firebaseDatabase.child("Messages").child(groupTitle).setValue(""){ (error, databaseRef) in
            if error == nil{
                handler(true,.FirebaseMessageGroupCreation)
            }else{
                handler(false,.FirebaseMessageNodeCreationError)
            }
        }
    }
    
    
    func updateGroup(groupTitle: String,message: [String: Any],handler: @escaping (Bool,FirebaseEnum) -> ()){
        firebaseDatabase.child("Groups").child(groupTitle).updateChildValues(message){[weak self] (error, databaseRef) in
            if error == nil{
                guard let strongSelf = self else { return }
                strongSelf.createMessages(groupTitle: groupTitle, handler: handler)
            }else{
                handler(false,.FirebaseGroupCreationError)
            }
        }
    }
    
    
    func sendMessage(messageID: String,message: [String: Any],handler: @escaping (Bool,FirebaseEnum) -> ()){
        firebaseDatabase.child("Messages").child(messageID).childByAutoId().setValue(message) {[weak self] (error, databaseRef) in
            if error == nil{
                guard let strongSelf = self else { return }
                strongSelf.updateGroup(groupTitle: messageID, message: ["LastMessage": message["message"] ?? "Image"], handler:handler)
                handler(true,.FirebaseMessageSendSuccess)
            }else{
                handler(false,.FirebaseMessageSendFailure)
            }
        }
    }
    
    //TODO:-
    func uploadImage(messageID: String,imageData: Data,message: [String: Any],handler: @escaping (Bool,FirebaseEnum) -> ()){
        var messageMetaData = message
        let filepath = firebaseStorage.child(messageID).child(UUID().uuidString + "/image_message.jpg")
       
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpg"
        filepath.putData(imageData, metadata: metaData) { (storageMeta, error) in
            if error == nil{
                filepath.downloadURL(completion: { (downloadURL, error) in
                    if error == nil{
                        guard let imageURL = downloadURL else { return }
                         messageMetaData["contentLocation"] = imageURL.absoluteString
                        self.sendMessage(messageID: messageID, message: messageMetaData, handler: handler)
                    }else{
                        handler(false,.FirebaseMessageSendFailure)
                    }
                })
               
            }else{
                print(error!.localizedDescription)
                handler(false,.FirebaseMessageSendFailure)
            }
        }
        
    }
    
    
    func readAllMessages(messageID: String,handler: @escaping (Bool,FirebaseEnum,[Messages]?) -> ()){
        firebaseDatabase.child("Messages").child(messageID).observeSingleEvent(of: DataEventType.value) { (snapshot) in
            var messages: [Messages] = []
            if let snap = snapshot.value as? [String: [String: Any]]{
                for (_,value) in snap{
                    let jsonRequest = try! JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                    let message =   try! JSONDecoder().decode(Messages.self, from: jsonRequest)
                    messages.append(message)
                }
                handler(true,.FirebaseMessageRead,messages)
            }else{
                handler(false,.FirebaseMessageReadFailure,nil)
            }
        }
        
    
        
      //  detachListener(handle: myHandle)
        
    }
    
    
    func readMessages(messageID: String,handler: @escaping (Bool,FirebaseEnum,Messages?) -> ()){
        firebaseDatabase.child("Messages").child(messageID).observe(DataEventType.childAdded) { (snapshot) in
            var message: Messages?
            if let snap = snapshot.value as? [String: Any]{
//                for (_,value) in snap{
                    let jsonRequest = try! JSONSerialization.data(withJSONObject: snap, options: .prettyPrinted)
                    message =   try! JSONDecoder().decode(Messages.self, from: jsonRequest)
                  
//                }
                handler(true,.FirebaseMessageRead,message)
            }else{
                handler(false,.FirebaseMessageReadFailure,nil)
            }
        }
    }
    
    func detachListener(handle: UInt){
         print("Detach Called")
         firebaseDatabase.removeObserver(withHandle:handle)
    }
    
    
}


enum FirebaseEnum: String{
    case FirebaseLoginError = "Error Logging in the user"
    case FirebaseUserError = "Error Creating the user"
    case FirebaseDatabaseUserError = "Error Inserting user in database"
    case FirebaseUserSuccess = "User inserted in the database successfully!!!"
    case FirebaseDatabaseUserUpdateError = "Error Updating user in database"
    case FirebaseUserUpdateSuccess = "User updated in the database successfully!!!"
    case FirebaseUsers = "Users"
    case fireLastSeen = "lastSeen"
    case FireUserLoginStatus = "User is currently logged in"
    case FireUserLoginFailStatus = "User isnt logged in"
    case FirebaseUsersListSuccess = "Users retrived Successfully"
    case FirebaseUsersListFailure = "Users retrived Failed"
    case FirebaseUsersLogoutFailure = "Users logout Failed"
    case FirebaseUsersLogoutSuccess = "Users logout Success"
    case FirebaseGroupCreationError = "Error in creating a group"
    case FirebaseMessageNodeCreationError = "Error in creating a Message Node"
    case FirebaseMessageGroupCreation = "New Group & Message Created Successfully!!!"
    case FirebaseMessageRead = "Message List Read Successfully!!!"
    case FirebaseMessageReadFailure = "Message List Read Fail!!!"
    case FirebaseMessageSendSuccess = "Message Sent Successfully!!"
    case FirebaseMessageSendFailure = "Message Sending failure!!"
    case FirebaseUploadImageFailure = "Image Upload failure!!"
}
