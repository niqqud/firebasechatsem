//
//  ViewController.swift
//  FirebaseChat
//
//  Created by RoHIT on 27/05/19.
//  Copyright © 2019 RoHIT. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {
    @IBOutlet weak var emailLabel: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        FirebaseService.shared.checkLoginStatus {[weak self] (result, firebaseResponce) in
            if result{
                 Utils.shared.log(data: firebaseResponce.rawValue)
               guard let strongSelf = self else { return }
               strongSelf.openDashBoard()
            }else{
                 Utils.shared.log(data: firebaseResponce.rawValue)
            }
        }
    }
    
    @IBAction func onLoginTapped(){
        signInUser(email: emailLabel.text!)
    }
    
    @IBAction func onSignUpTapped(){
        openSignUp()
    }

    func signInUser(email: String){
        FirebaseService.shared.loginUser(emailID: email) {[weak self] (result, firebaseResponce,userID) in
            if result{
                 Utils.shared.log(data: firebaseResponce.rawValue)
                guard let strongSelf = self else { return }
                 UserDefaults.standard.set(userID, forKey: "UserID")
                strongSelf.openDashBoard()
            }else{
                Utils.shared.log(data: firebaseResponce.rawValue)
            }
        }
    }

    func openDashBoard(){
        guard let viewController = navigationController?.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as? TabBarController else { return }
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func openSignUp(){
        guard let viewController = navigationController?.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController else { return }
        navigationController?.pushViewController(viewController, animated: true)
    }

    
}

