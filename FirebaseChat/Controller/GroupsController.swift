//
//  GroupsController.swift
//  FirebaseChat
//
//  Created by RoHIT on 28/05/19.
//  Copyright © 2019 RoHIT. All rights reserved.
//

import UIKit

class GroupsController: UIViewController {
    var groups: [Groups] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        FirebaseService.shared.getGroupsList(UserID: UserDefaults.standard.string(forKey: "UserID")!) {[weak self] (result, firebaseRef, groups) in
            if result {
                guard let strongSelf = self else { return }
                guard let userGroups = groups else { return }
                strongSelf.groups = userGroups
                strongSelf.tableView.reloadData()
            }else{
                print("Opps")
            }
        }
    }
    

    func openMessages(messageID: String,group: Groups){
        let messageController = navigationController?.storyboard?.instantiateViewController(withIdentifier: "MessageController") as! MessageController
        messageController.messageID = messageID
        messageController.group = group
        navigationController?.pushViewController(messageController, animated: true)
    }


}

extension GroupsController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupCell", for: indexPath) as? GroupCell
        cell?.groupDetails = groups[indexPath.row]
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.openMessages(messageID: groups[indexPath.row].message,group: groups[indexPath.row])
    }
    
}
