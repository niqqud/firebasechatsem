//
//  UsersViewController.swift
//  FirebaseChat
//
//  Created by RoHIT on 28/05/19.
//  Copyright © 2019 RoHIT. All rights reserved.
//

import UIKit

class UsersViewController: UIViewController {
    var users: [Users] = []
    @IBOutlet weak var tableView: UITableView!
    var currentUser: Users?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        guard let userID = UserDefaults.standard.string(forKey: "UserID") else { return }
        
        FirebaseService.shared.getUsersList {[weak self] (result, firebaseResponce, usersList) in
            if result,let userList = usersList{
                print(userList)
                guard let strongSelf = self else { return }
                strongSelf.users = userList
                strongSelf.tableView.reloadData()
                //TODO:- Add Some better logic
                for value in userList{
                    if value.userID == userID{
                        strongSelf.currentUser = value
                    }
                }
                
            }else{
                Utils.shared.log(data:"Opps")
            }
        }
    }
    
    
    @IBAction func onLogout(){
        FirebaseService.shared.logoutUser {[weak self] (result, response) in
            if result{
                guard let strongSelf = self else {return}
                strongSelf.navigationController?.popToRootViewController(animated: true)
            }else{
                print("Oppsi")
            }
        }
    }
    

}

extension UsersViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as? UserCell
        cell?.users = users[indexPath.row]
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let currentUser = currentUser else {return}
        let otherUser = users[indexPath.row]
        let groupID = "\(currentUser.userID)-\(otherUser.userID)"
        //TODO:- Fix the exception
        let group = try! Groups.init(country: [currentUser.country,otherUser.country], ID: [currentUser.userID,otherUser.userID], name: [currentUser.name,otherUser.name], email: [currentUser.email,otherUser.email], timestamp: 31231231, message: groupID, token: [currentUser.userID,otherUser.userID],lastMessage: nil).asDictionary()
        
        
        
        FirebaseService.shared.createGroup(groupTitle: groupID, group: group) {[weak self] (result, firebaseResponse) in
            if result{
                print("Result")
                guard let strongSelf = self else {return}
                strongSelf.navigationController?.pushViewController(strongSelf.navigationController?.storyboard?.instantiateViewController(withIdentifier: "GroupsController") as! GroupsController, animated: true)
            }else{
                print("Result False")
            }
        }
        
    }
    
    
}
