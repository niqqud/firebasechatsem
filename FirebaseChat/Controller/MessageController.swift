//
//  MessageController.swift
//  FirebaseChat
//
//  Created by RoHIT on 29/05/19.
//  Copyright © 2019 RoHIT. All rights reserved.
//

import UIKit

class MessageController: UIViewController {

    
    var onceMessageCheck: Bool = false
    var messageID: String?
    var group: Groups?
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var messageList: [Messages] = []
    var userID: String?
    
    let picker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        picker.delegate = self
         userID = UserDefaults.standard.string(forKey: "UserID")
        readMessageOnce()
        
    }
    
    
    
    func readMessageOnce(){
//        guard onceMessageCheck == false else { return }
//        onceMessageCheck.toggle()
        FirebaseService.shared.readAllMessages(messageID: messageID!) { (result, firebaseResponse, messageList) in
            if result{
                guard let messageList = messageList else {return}
               self.messageList = messageList
                self.tableView.reloadData()
               // self.readMessages()
            }else{
                print("Opppps")
            }
        }
            
        }
    
    
    
    func readMessages(){
        FirebaseService.shared.readMessages(messageID: messageID!) { (result, firebaseResponse, message) in
            if result{
                guard let message = message else {return}
                self.messageList.append(message)
                self.tableView.reloadData()
            }else{
                print("Opppps")
            }
        }
    }
    
    
    @IBAction func onUploadImage(_ sender: UIButton) {
          present(picker, animated: true)
    }
    
    func sendImageMessage(imageURL: UIImage){
        guard let messageID = messageID else { return }
        guard let group = group else { return }
        var messageList: [String: Any] = [:]
        
        let imageData = imageURL.jpegData(compressionQuality: 0.5)
        
        let message = self.messageTextView.text!
        let timeStamp =  Date().description
        let user =  messageID.components(separatedBy: "-")
        if userID == user[0]{
            messageList = try! Messages.init(contentType: "image", contentLocation: "", groupID: group.message, message: message, multimedia: false, senderName: group.name[0], senderEmail: group.email[0], senderCountry: group.country[0], receiverCountry: group.country[1], receiverToken: "XXXXX", timestamp:timeStamp,senderID: user[0],receiverID: user[1]).asDictionary()
            
        }else{
            messageList = try! Messages.init(contentType: "image", contentLocation: "", groupID: group.message, message: message, multimedia: false, senderName: group.name[1], senderEmail: group.email[1], senderCountry: group.country[1], receiverCountry: group.country[0], receiverToken: "XXXXX", timestamp:timeStamp,senderID: user[1],receiverID: user[0]).asDictionary()
        }
        
        FirebaseService.shared.uploadImage(messageID: messageID, imageData: imageData!, message: messageList) { (result, firebaseResponse) in
            if result{
                print("Image Sent Successfully")
            }else{
                print("Opps")
            }
        }
        
    }
    
    @IBAction func onSendMessageTapped(_ button: UIButton){
        //button.isEnabled = false
        guard let messageID = messageID else { return }
        guard let group = group else { return }
        var messageList: [String: Any] = [:]
        
       
        let message = self.messageTextView.text!
        let timeStamp =  Date().description
        let user =  messageID.components(separatedBy: "-")
        if userID == user[0]{
            messageList = try! Messages.init(contentType: "", contentLocation: "", groupID: group.message, message: message, multimedia: false, senderName: group.name[0], senderEmail: group.email[0], senderCountry: group.country[0], receiverCountry: group.country[1], receiverToken: "XXXXX", timestamp:timeStamp,senderID: user[0],receiverID: user[1]).asDictionary()
            
        }else{
            messageList = try! Messages.init(contentType: "", contentLocation: "", groupID: group.message, message: message, multimedia: false, senderName: group.name[1], senderEmail: group.email[1], senderCountry: group.country[1], receiverCountry: group.country[0], receiverToken: "XXXXX", timestamp:timeStamp,senderID: user[1],receiverID: user[0]).asDictionary()
        }
        
        FirebaseService.shared.sendMessage(messageID: messageID, message: messageList) { (result, firebaseResponse) in
            if result{
                print("Message Sent Successfully")
            }else{
                print("Opps")
            }
        }
        
        
    }
    
    
    
}

extension MessageController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        if messageList[indexPath.row].senderID == userID!{
        let sendMessageCell = tableView.dequeueReusableCell(withIdentifier: "SendMessageCell", for: indexPath) as! SendMessageCell
            sendMessageCell.message = messageList[indexPath.row]
            cell = sendMessageCell
        }else{
            let receiveMessageCell = tableView.dequeueReusableCell(withIdentifier: "ReceiveMessageCell", for: indexPath) as! ReceiveMessageCell
             receiveMessageCell.message = messageList[indexPath.row]
            cell = receiveMessageCell
        }
        return cell
    }
    
    
}


extension MessageController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else { return }
        sendImageMessage(imageURL: image)
    }
    
}
