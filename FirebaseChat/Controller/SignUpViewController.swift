//
//  SignUpViewController.swift
//  FirebaseChat
//
//  Created by RoHIT on 27/05/19.
//  Copyright © 2019 RoHIT. All rights reserved.
//

import UIKit
import FirebaseAuth

import FirebaseDatabase
class SignUpViewController: UIViewController {
    @IBOutlet weak var emailLabel: UITextField!
    @IBOutlet weak var nameLabel: UITextField!
    @IBOutlet weak var countryLabel: UITextField!
    @IBOutlet weak var interestLabel: UITextField!
    @IBOutlet weak var phoneNumberLabel: UITextField!

    @IBOutlet weak var language: UITextField!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //TODO:- add validation before this
    func getUserInfo() -> [String: Any]{
        let userInfo = Users.init(ID: "XYZ", name: nameLabel.text!, email: emailLabel.text!, country: countryLabel.text!, groups: ["NewBie","StartUp"], interest: ["Sports","Cricket"], language: language.text!, languageCode: "EN", lastSeen: Date().description, phone: phoneNumberLabel.text!, token: "XXXXX",userID: "")
        
//        let data: [String: Any] = [
//            "ID": "XYZ",
//            "Name": nameLabel.text!,
//            "Email": emailLabel.text!,
//            "Country": countryLabel.text!,
//            "Groups": ["NewBie","StartUp"],
//            "Interest": ["Sports","Cricket"],
//            "Language": language.text!,
//            "LanguageCode": "EN",
//            "LastSeen": Date().description,
//            "Phone": phoneNumberLabel.text!,
//            "Token": "XXXXXXXX"
//        ]
        

        
        
        
        return try! userInfo.asDictionary()
    }
    
    
    @IBAction func onSignUpTapped(_ button: UIButton){
        FirebaseService.shared.createUser(emailID: emailLabel.text!, userInfo: getUserInfo()){ [weak self] (result,firebaseResponce,userID) in
            if result{
                Utils.shared.log(data: firebaseResponce.rawValue)
                guard let strongSelf = self else { return }
                strongSelf.openDashBoard()
               UserDefaults.standard.set(userID, forKey: "UserID")
            }else{
                Utils.shared.log(data: firebaseResponce.rawValue)
            }
        }
    }

    
    func openDashBoard(){
        guard let viewController = navigationController?.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as? TabBarController else { return }
        navigationController?.pushViewController(viewController, animated: true)
    }

 
    
}
