//
//  Users.swift
//  FirebaseChat
//
//  Created by RoHIT on 28/05/19.
//  Copyright © 2019 RoHIT. All rights reserved.
//

import Foundation
struct Users: Codable {
    
//    let data: [String: Any] = [
//        "ID": "XYZ",
//        "Name": nameLabel.text!,
//        "Email": emailLabel.text!,
//        "Country": countryLabel.text!,
//        "Groups": ["NewBie","StartUp"],
//        "Interest": ["Sports","Cricket"],
//        "Language": language.text!,
//        "LanguageCode": "EN",
//        "LastSeen": Date().description,
//        "Phone": phoneNumberLabel.text!,
//        "Token": "XXXXXXXX"
//    ]
    
//
//    let ID: String?
//    let Name: String?
//    let Email: String?
//    let Country: String?
//    let Groups: [String]?
//    let Interest: [String]?
//    let Language: String?
//    let LanguageCode: String?
//    let LastSeen: String?
//    let Phone: String?
//    let Token: String?

    
    
    let ID: String
    let name: String
    let email: String
    let country: String
    let groups: [String]
    let interest: [String]
    let language: String
    let languageCode: String
    let lastSeen: String
    let phone: String
    let token: String
    let userID: String
    
    enum CodingKeys: String, CodingKey {
        case ID = "ID"
        case name = "Name"
        case email = "Email"
        case country = "Country"
        case groups = "Groups"
        case interest = "Interest"
        case language = "Language"
        case languageCode = "LanguageCode"
        case lastSeen = "LastSeen"
        case phone = "Phone"
        case token = "Token"
        case userID = "UID"
    }
    
}


struct Groups: Codable {
    let country: [String]
    let ID: [String]
    let name: [String]
    let email: [String]
    let timestamp: Int
    let message: String
    let token: [String]
    let lastMessage: String?
    enum CodingKeys: String, CodingKey {
        case ID = "GID"
        case name = "Name"
        case email = "Email"
        case country = "Country"
        case message = "Message"
        case token = "Token"
        case timestamp = "UNIXSTAMP"
        case lastMessage = "LastMessage"
    }
}



struct Messages: Codable{
    let contentType: String
    let contentLocation: String
    let groupID: String
    let message: String
    let multimedia: Bool
    let senderName: String
    let senderEmail: String
    let senderCountry: String
    let receiverCountry: String
    let receiverToken: String
    let timestamp: String
    let senderID: String
    let receiverID: String
}


extension Encodable {
    func asDictionary() throws -> [String: Any] {
        let data = try JSONEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }
}
