//
//  GroupCell.swift
//  FirebaseChat
//
//  Created by RoHIT on 29/05/19.
//  Copyright © 2019 RoHIT. All rights reserved.
//

import UIKit

class GroupCell: UITableViewCell {
    @IBOutlet weak var usersname: UILabel!
    @IBOutlet weak var lastSeen: UILabel!
    @IBOutlet weak var lastMessage: UILabel!
    
    var groupDetails: Groups?{
        willSet{
            guard let name = newValue?.name[1] else {return}
            usersname.text = name
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
