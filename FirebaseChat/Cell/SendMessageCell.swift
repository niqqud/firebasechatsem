//
//  MessageCell.swift
//  FirebaseChat
//
//  Created by RoHIT on 29/05/19.
//  Copyright © 2019 RoHIT. All rights reserved.
//

import UIKit

class SendMessageCell: UITableViewCell {

    @IBOutlet weak var sendername: UILabel!
    @IBOutlet weak var senderTextLabel: UILabel!
    
    var message: Messages?{
        willSet{
            guard let text = newValue?.message else { return }
            senderTextLabel.text = text
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
