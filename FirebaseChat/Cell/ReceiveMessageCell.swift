//
//  ReceiveMessageCell.swift
//  FirebaseChat
//
//  Created by RoHIT on 29/05/19.
//  Copyright © 2019 RoHIT. All rights reserved.
//

import UIKit

class ReceiveMessageCell: UITableViewCell {

    @IBOutlet weak var receiverUserLabel: UILabel!
    
    @IBOutlet weak var reveiverTextLabel: UILabel!
    var message: Messages?{
        willSet{
            guard let text = newValue?.message else { return }
            reveiverTextLabel.text = text
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
